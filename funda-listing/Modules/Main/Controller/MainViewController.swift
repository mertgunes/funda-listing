import UIKit
import RxSwift

final class MainViewController: BaseTableViewController {
	private let viewModel: MainViewModel
	
	private var estateAgentListingCounts: [EstateAgentListingCount] = [] {
		didSet {
			contentView.tableView.reloadData()
		}
	}
	
	// MARK: - Initializers
	required init?(coder aDecoder: NSCoder) {
		preconditionFailure("init(coder:) has not been implemented")
	}
	
	init(viewModel: MainViewModel) {
		self.viewModel = viewModel
		super.init()
		
		contentView.delegate = self
		contentView.tableView.register(MainViewCell.self, forCellReuseIdentifier: MainViewCell.identifier)
		contentView.tableView.dataSource = self
		contentView.tableView.rowHeight = 62.0
	}
	
	// MARK: - View Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		bind(to: viewModel)
		viewModel.inputs.viewDidEndLoading()
	}
}

// MARK: - View Model
private extension MainViewController {
	func bind(to viewModel: MainViewModel) {
		viewModel.outputs.navigationTitle
			.subscribe(
				onNext: { [weak self] title in
					self?.navigationItem.title = title
				}
			)
			.disposed(by: disposeBag)
		
		viewModel.outputs.state
			.observeOn(MainScheduler.instance)
			.subscribe(
				onNext: { [weak self] state in
					self?.contentView.state = state
				}
			)
			.disposed(by: disposeBag)
		
		viewModel.outputs.estateAgentListingCounts
			.subscribe(
				onNext: { [weak self] counts in
					self?.estateAgentListingCounts = counts
				}
			)
			.disposed(by: disposeBag)
	}
}

// MARK: - Configuration
private extension MainViewController {
	func configure(_ cell: MainViewCell, forRowAt indexPath: IndexPath) {
		guard indexPath.row < estateAgentListingCounts.count else { return }
		let listingCount = estateAgentListingCounts[indexPath.row]
		cell.textLabel?.text = listingCount.estateAgent.name
		cell.detailTextLabel?.text = "\(listingCount.count)"
	}
}

// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return estateAgentListingCounts.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: MainViewCell.identifier,
													   for: indexPath) as? MainViewCell else {
			return UITableViewCell()
		}
		configure(cell, forRowAt: indexPath)
		return cell
	}
}

// MARK: - StatefulTableViewDelegate
extension MainViewController: StatefulTableViewDelegate {
	func statefulTableView(_ view: StatefulTableView, didTrigger action: StatefulTableView.DelegateAction) {
		switch action {
		case .didReceiveTapOnErrorStateView:
			viewModel.inputs.didReceiveTapOnErrorStateView()
		default:
			break
		}
	}
}
