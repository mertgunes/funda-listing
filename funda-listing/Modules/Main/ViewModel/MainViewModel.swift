import Foundation
import RxSwift

final class DefaultMainViewModel: MainViewModel {
	var inputs: MainViewModelInputs { return self }
	var outputs: MainViewModelOutputs { return self }
	
	private lazy var estateAgentListingCountsInput: BehaviorSubject<[EstateAgentListingCount]> = {
		return BehaviorSubject<[EstateAgentListingCount]>(value: [])
	}()
	
	private lazy var navigationTitleInput: BehaviorSubject<String?> = {
		return BehaviorSubject<String?>(value: nil)
	}()
	
	private lazy var stateInput: BehaviorSubject<StatefulTableView.State> = {
		return BehaviorSubject<StatefulTableView.State>(value: .idle)
	}()
	
	private let estateAgentRankingManager: EstateAgentRankingManager
	
	lazy var disposeBag = DisposeBag()
	
	init(with manager: EstateAgentRankingManager) {
		self.estateAgentRankingManager = manager
		
		estateAgentRankingManager.title
			.observeOn(MainScheduler.instance)
			.subscribe(
				onNext: { [weak self] title in
					self?.navigationTitleInput.onNext(title)
				}
			)
			.disposed(by: disposeBag)
		
		estateAgentRankingManager.progress
			.observeOn(MainScheduler.instance)
			.subscribe(
				onNext: { [weak self] progress in
					let currentProgress = "Fetching Data\nCurrent Progress\n%\(String(format: "%.2f", progress * 100))"
					let state = StatefulTableView.State.empty(currentProgress)
					self?.stateInput.onNext(state)
				}
			)
			.disposed(by: disposeBag)
		
		estateAgentRankingManager.errorMessage
			.observeOn(MainScheduler.instance)
			.subscribe(
				onNext: { [weak self] errorMessage in
					let state = StatefulTableView.State.error("\(errorMessage)\nTap to try again!")
					self?.stateInput.onNext(state)
				}
			)
			.disposed(by: disposeBag)
		
		estateAgentRankingManager.estateAgentListingCounts
			.observeOn(MainScheduler.instance)
			.subscribe(
				onNext: { [weak self] counts in
					self?.stateInput.onNext(.idle)
					self?.estateAgentListingCountsInput.onNext(counts)
				}
			)
			.disposed(by: disposeBag)
	}
}

// MARK: - Inputs
extension DefaultMainViewModel: MainViewModelInputs {
	func viewDidEndLoading() {
		estateAgentRankingManager.startFetching()
	}
	
	func didReceiveTapOnErrorStateView() {
		estateAgentRankingManager.startFetching()
	}
}

// MARK: - Outputs
extension DefaultMainViewModel: MainViewModelOutputs {
	var estateAgentListingCounts: Observable<[EstateAgentListingCount]> { return estateAgentListingCountsInput.asObservable() }
	var navigationTitle: Observable<String?> { return navigationTitleInput.asObservable() }
	var state: Observable<StatefulTableView.State> { return stateInput.asObservable() }
}

// MARK: - Protocols
protocol MainViewModel {
	var inputs: MainViewModelInputs { get }
	var outputs: MainViewModelOutputs { get }
}

protocol MainViewModelInputs {
	func viewDidEndLoading()
	func didReceiveTapOnErrorStateView()
}

protocol MainViewModelOutputs {
	var estateAgentListingCounts: Observable<[EstateAgentListingCount]> { get }
	var navigationTitle: Observable<String?> { get }
	var state: Observable<StatefulTableView.State> { get }
}
