import UIKit

final class MainCoordinator: Coordinator {
	
	var viewController: UIViewController {
		return navigationController
	}
	
	private lazy var navigationController: BaseNavigationController = {
		return BaseNavigationController(rootViewController: mainViewController)
	}()
	
	private lazy var mainViewController: MainViewController = {
		let viewModel = DefaultMainViewModel(with: estateAgentRankingManager)
		return MainViewController(viewModel: viewModel)
	}()
	
	private let estateAgentRankingManager: EstateAgentRankingManager
	
	// MARK: - Initializers
	init(estateAgentRankingManager: EstateAgentRankingManager) {
		self.estateAgentRankingManager = estateAgentRankingManager
	}
}

