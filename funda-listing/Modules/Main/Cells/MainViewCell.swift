import UIKit

final class MainViewCell: UITableViewCell {
	// MARK: - Initializers
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: UITableViewCell.CellStyle.value1, reuseIdentifier: reuseIdentifier)
		selectionStyle = UITableViewCell.SelectionStyle.none
		textLabel?.numberOfLines = 2
	}
}
