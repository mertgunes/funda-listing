import UIKit

final class RootCoordinator: Coordinator {
	private let repository: Repository
	
	lazy private(set) var viewController: UIViewController = {
		return BaseViewController()
	}()
	
	private lazy var mainCoordinator: MainCoordinator = {
		let rankingManager = DefaultEstateAgentRankingManager(repository: repository,
															  location: "amsterdam",
															  type: .purchase,
															  filter: .garden)
		return MainCoordinator(estateAgentRankingManager: rankingManager)
	}()
	
	// MARK: - Initializers
	init(repository: Repository) {
		self.repository = repository
		addChildCoordinator(mainCoordinator)
	}
	
	// MARK: - Coordination Methods
	private func addChildCoordinator(_ coordinator: Coordinator) {
		DispatchQueue.main.async { [weak self] in
			guard let strongSelf = self else { return }
			if strongSelf.viewController.children.contains(coordinator.viewController) { return }
			strongSelf.viewController.addChild(coordinator.viewController)
			strongSelf.viewController.view.addSubview(coordinator.viewController.view)
			coordinator.viewController.view.bindFrameToSuperviewBounds()
			coordinator.viewController.didMove(toParent: strongSelf.viewController)
		}
	}
	
	private func removeChildCoordinator(_ coordinator: Coordinator) {
		DispatchQueue.main.async {
			coordinator.viewController.willMove(toParent: nil)
			coordinator.viewController.view.removeFromSuperview()
			coordinator.viewController.removeFromParent()
		}
	}
}
