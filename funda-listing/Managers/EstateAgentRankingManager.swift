import Foundation
import RxSwift

protocol EstateAgentRankingManager {
	func startFetching()
	
	var title: Observable<String> { get }
	var progress: Observable<CGFloat> { get }
	var errorMessage: Observable<String> { get }
	var estateAgentListingCounts: Observable<[EstateAgentListingCount]> { get }
}

final class DefaultEstateAgentRankingManager: EstateAgentRankingManager {
	var title: Observable<String> { return titleInput.asObservable() }
	var progress: Observable<CGFloat> { return progressInput.asObservable() }
	var errorMessage: Observable<String> { return errorMessageInput.asObservable() }
	var estateAgentListingCounts: Observable<[EstateAgentListingCount]> { return estateAgentListingCountsInput.asObservable() }
	
	private let fetchRentalListingsInput: PublishSubject<FetchRentalListingsDraft> = PublishSubject()
	
	private lazy var titleInput: PublishSubject<String> = {
		return PublishSubject<String>()
	}()
	
	private lazy var progressInput: BehaviorSubject<CGFloat> = {
		return BehaviorSubject<CGFloat>(value: 0.0)
	}()
	
	private lazy var errorMessageInput: PublishSubject<String> = {
		return PublishSubject<String>()
	}()
	
	private lazy var estateAgentListingCountsInput: BehaviorSubject<[EstateAgentListingCount]> = {
		return BehaviorSubject<[EstateAgentListingCount]>(value: [])
	}()
	
	private var fetchedListings = [Int: [EstateAgent]]()
	private var draft: FetchRentalListingsDraft
	private var pagination: Pagination = Pagination()
	
	private let repository: Repository
	private let location: String
	private let type: FetchRentalListingsDraft.ListingType
	private let filter: FetchRentalListingsDraft.Filter?
	
	private let disposeBag = DisposeBag()
	
	// MARK: - Initializers
	init(repository: Repository,
		 location: String,
		 type: FetchRentalListingsDraft.ListingType,
		 filter: FetchRentalListingsDraft.Filter?) {
		self.repository = repository
		self.location = location
		self.type = type
		self.filter = filter
		
		self.draft = FetchRentalListingsDraft(location: location,
											  type: type,
											  filter: filter,
											  pagination: pagination)
		
		let fetchRentalListing = fetchRentalListingsInput.flatMap { draft in
			repository
				.fetchRentalListings(with: draft)
				.retry(3)
				.asObservable()
				.materialize()
			}.share()
		
		fetchRentalListing.elements()
			.observeOn(MainScheduler.instance)
			.subscribe(
				onNext: { [weak self] bundle in
					self?.didFetchBundle(bundle)
				}
			)
			.disposed(by: disposeBag)
		
		fetchRentalListing.errors()
			.observeOn(MainScheduler.instance)
			.subscribe(
				onNext: { [weak self] error in
					self?.errorMessageInput.onNext(error.localizedDescription)
				}
			)
			.disposed(by: disposeBag)
	}
	
	// MARK: - Helper Methods
	private func didFetchBundle(_ bundle: ListResponseBundle<RentalListing>) {
		fetchedListings[bundle.pagination.currentPage] = bundle.objects.compactMap({ $0.estateAgent })
		
		let currentProgress = CGFloat(bundle.pagination.currentPage) / CGFloat(bundle.pagination.totalPageCount)
		progressInput.onNext(currentProgress)
		
		if bundle.pagination.isLastPage {
			generateRankings()
		} else {
			pagination = bundle.pagination
			draft = FetchRentalListingsDraft(location: location,
											 type: type,
											 filter: filter,
											 pagination: pagination)
			fetchRentalListingsInput.onNext(draft)
		}
	}
	
	private func generateRankings() {
		let mappedItems = fetchedListings.compactMap({ $0.value }).reduce([], +).map { ($0, 1) }
		let counts = Dictionary(mappedItems, uniquingKeysWith: +)
			.sorted { $0.1 > $1.1 }[0..<10]
			.compactMap({ EstateAgentListingCount(estateAgent: $0.key, count: $0.value) })
		
		estateAgentListingCountsInput.onNext(counts)
	}
	
	// MARK: - Protocol Methods
	func startFetching() {
		fetchRentalListingsInput.onNext(draft)
		
		titleInput.onNext([location.localizedCapitalized, type.rawValue, filter?.rawValue]
			.compactMap({ $0 })
			.joined(separator: " | "))
	}
}
