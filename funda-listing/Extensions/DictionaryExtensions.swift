import Foundation

infix operator <+ : AdditionPrecedence

extension Dictionary {
	static func <+(_ left: Dictionary, _ right: Dictionary) -> Dictionary {
		var result = left
		right.forEach({ result[$0.key] = $0.value })
		return result
	}
}
