import Foundation

extension JSONDecoder {
	static var funda: JSONDecoder {
		let dec = JSONDecoder()
		dec.dateDecodingStrategy = .iso8601
		return dec
	}
}
