import Foundation
import RxSwift

private let pathPrefix: String = "/Aanbod.svc/json/%@/"

protocol APIClient {
	func fetchRentalListings(with draft: FetchRentalListingsDraft) -> Single<Data>
}

struct DefaultAPIClient: APIClient {
	private let networkClient: NetworkClient
	private let apiKey: String
	
	init(networkClient: NetworkClient, and key: String) {
		self.networkClient = networkClient
		self.apiKey = key
	}
	
	func fetchRentalListings(with draft: FetchRentalListingsDraft) -> Single<Data> {
		let payload = RequestPayload.query(draft.parameters())
		let path = String(format: Path.feeds.rawPathValue, apiKey)
		
		let endpoint = DefaultAPIEndpoint(path: path,
										  httpMethod: HttpMethod.get,
										  requestPayload: payload)
		return networkClient.fetch(endpoint)
	}
}

// MARK: - Path
private extension DefaultAPIClient {
	enum Path: String {
		case feeds	= "/feeds"
		
		var rawPathValue: String {
			return "/\(rawValue)\(pathPrefix)"
		}
	}
}
