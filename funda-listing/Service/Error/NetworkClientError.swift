import Foundation

enum NetworkClientErrorType {
	case invalidURL(String)
	case invalidResponse(URLResponse?, Data?)
	case noInternetConnection
	case apiError(APIError)
	case unknownError
	
	var localizedDescription: String {
		switch self {
		case .apiError(let error):
			return error.message
		case .noInternetConnection:
			return NSLocalizedString("Check your connection and try again.", comment: "")
		default:
			return NSLocalizedString("An unknown error occurred.", comment: "")
		}
	}
}

class NetworkClientError: NSObject, LocalizedError {
	let type: NetworkClientErrorType
	
	init(type: NetworkClientErrorType) {
		self.type = type
	}
	
	override var description: String {
		return type.localizedDescription
	}
	
	var errorDescription: String? {
		return description
	}
}
