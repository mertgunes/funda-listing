import Foundation
import RxSwift

protocol Repository {
	func fetchRentalListings(with draft: FetchRentalListingsDraft) -> Single<ListResponseBundle<RentalListing>>
}

final class DefaultRepository: Repository {
	let client: APIClient
	
	init(with client: APIClient) {
		self.client = client
	}
	
	func fetchRentalListings(with draft: FetchRentalListingsDraft) -> Single<ListResponseBundle<RentalListing>> {
		return client.fetchRentalListings(with: draft).map {
			return try JSONDecoder.funda.decode(ListResponseBundle<RentalListing>.self, from: $0)
		}
	}
}
