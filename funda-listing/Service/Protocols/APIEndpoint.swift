import Foundation

enum RequestPayload {
	case query([String: Any])
	case form([String: Any])
	case body(Any)
	case data(Data)
}

enum HeaderParameter {
	case contentType(String)
	
	var key: String {
		switch self {
		case .contentType:
			return "Content-Type"
		}
	}
	
	var value: String {
		switch self {
		case .contentType(let value):
			return value
		}
	}
}

protocol APIEndpoint {
	var path: String { get }
	var httpMethod: HttpMethod { get }
	var requestPayload: RequestPayload? { get }
	var headerParameters: [HeaderParameter]? { get }
	
	init(path: String,
		 httpMethod: HttpMethod,
		 requestPayload: RequestPayload?,
		 headerParameters: [HeaderParameter]?)
}

struct DefaultAPIEndpoint: APIEndpoint {
	let path: String
	let httpMethod: HttpMethod
	let requestPayload: RequestPayload?
	var headerParameters: [HeaderParameter]?
	
	init(path: String,
		 httpMethod: HttpMethod,
		 requestPayload: RequestPayload? = nil,
		 headerParameters: [HeaderParameter]? = nil) {
		self.path = path
		self.httpMethod = httpMethod
		self.requestPayload = requestPayload
		self.headerParameters = headerParameters
	}
}
