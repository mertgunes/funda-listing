import Foundation
import UIKit

protocol APIAdapter {
	var request: URLRequest? { get }
	var endpoint: APIEndpoint { get }
	var host: String { get }
	var defaultHeaderParameters: [HeaderParameter] { get }
	
	init(with host: String, and endpoint: APIEndpoint)
}

extension APIAdapter {
	var request: URLRequest? {
		guard var components = URLComponents(string: host) else { return nil }
		components.path = endpoint.path
		
		if let payload = endpoint.requestPayload {
			switch payload {
			case .query(let items):
				components.queryItems = items.map {
					URLQueryItem(name: $0.key, value: "\($0.value)")
				}
				let query = components.query
				var characters = CharacterSet.urlQueryAllowed
				characters.remove("+")
				let encodedQuery = query?.addingPercentEncoding(withAllowedCharacters: characters)
				components.percentEncodedQuery = encodedQuery
			default:
				break
			}
		}
		
		guard let url = components.url else { return nil }
		var request = URLRequest(url: url)
		request.httpMethod = endpoint.httpMethod.value
		
		let headerParameters = defaultHeaderParameters + (endpoint.headerParameters ?? [])
		let allHTTPHeaderFields = Dictionary(uniqueKeysWithValues: headerParameters.map{ ($0.key, $0.value) })
		request.allHTTPHeaderFields = allHTTPHeaderFields
		
		if let payload = endpoint.requestPayload {
			switch payload {
			case .form(let items):
				var formComponents = URLComponents()
				formComponents.queryItems = items.map { URLQueryItem(name: $0.key, value: "\($0.value)") }
				request.httpBody = formComponents.query?.data(using: String.Encoding.utf8, allowLossyConversion: false)
			case .body(let body):
				do {
					request.httpBody = try JSONSerialization.data(withJSONObject: body, options: [])
				} catch {
					
				}
			case .data(let data):
				request.httpBody = data
			default:
				break
			}
		}
		
		return request
	}
	
	var defaultHeaderParameters: [HeaderParameter] {
		var fields = [HeaderParameter]()
		
		if let payload = endpoint.requestPayload {
			switch payload {
			case .body, .data:
				fields.append(HeaderParameter.contentType("application/json"))
			case .form:
				fields.append(HeaderParameter.contentType("application/x-www-form-urlencoded"))
			default:
				break
			}
		}
		
		return fields
	}
}

struct DefaultAPIAdapter: APIAdapter {
	var endpoint: APIEndpoint
	var host: String
	
	init(with host: String, and endpoint: APIEndpoint) {
		self.endpoint = endpoint
		self.host = host
	}
}
