import UIKit
import SnapKit

final class StatefulTableView: UIView {
	weak var delegate: StatefulTableViewDelegate?
	
	lazy var tableView: UITableView = {
		let tableView = UITableView()
		tableView.backgroundColor = ColorPalette.primaryBackground.colorValue
		tableView.keyboardDismissMode = .interactive
		tableView.separatorInset = .zero
		tableView.tableFooterView = UIView(frame: .zero)
		return tableView
	}()
	
	private lazy var emptyStateView: EmptyStateView = {
		let view = EmptyStateView()
		view.delegate = self
		return view
	}()
	
	private lazy var errorStateView: ErrorStateView = {
		let view = ErrorStateView()
		view.delegate = self
		return view
	}()
	
	private lazy var loadingView = LoadingStateView()
	
	var state: State = .idle {
		didSet {
			updateInterface(with: state)
		}
	}
	
	// MARK: - Initializers
	required init?(coder aDecoder: NSCoder) {
		preconditionFailure("init(coder:) has not been implemented")
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = ColorPalette.primaryBackground.colorValue
		
		addSubview(tableView)
		tableView.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
		
		addSubview(emptyStateView)
		emptyStateView.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
		
		addSubview(errorStateView)
		errorStateView.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
		
		addSubview(loadingView)
		loadingView.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
		
		updateInterface(with: state)
	}
}

// MARK: - State
extension StatefulTableView {
	enum State {
		case idle
		case loading
		case empty(String)
		case error(String)
	}
	
	private func updateInterface(with state: State) {
		[emptyStateView, errorStateView, loadingView, tableView].forEach({
			$0.isHidden = true
		})
		
		loadingView.stopAnimating()
		
		switch state {
		case .empty(let text):
			emptyStateView.configure(with: text)
			emptyStateView.isHidden = false
		case .error(let text):
			errorStateView.configure(with: text)
			errorStateView.isHidden = false
		case .loading:
			loadingView.isHidden = false
			loadingView.startAnimating()
		case .idle:
			tableView.isHidden = false
		}
	}
}

// MARK: - StatefulTableViewDelegate
extension StatefulTableView {
	
	enum DelegateAction {
		case didReceiveTapOnEmptyStateView
		case didReceiveTapOnErrorStateView
	}
}

protocol StatefulTableViewDelegate: class {
	func statefulTableView(_ view: StatefulTableView,
						   didTrigger action: StatefulTableView.DelegateAction)
}

// MARK: - EmptyStateViewDelegate
extension StatefulTableView: EmptyStateViewDelegate {
	func emptyStateView(_ view: EmptyStateView, didTrigger action: EmptyStateView.DelegateAction) {
		switch action {
		case .didReceiveTap:
			delegate?.statefulTableView(self, didTrigger: .didReceiveTapOnEmptyStateView)
		}
	}
}

// MARK: - ErrorStateViewDelegate
extension StatefulTableView: ErrorStateViewDelegate {
	func errorStateView(_ view: ErrorStateView, didTrigger action: ErrorStateView.DelegateAction) {
		switch action {
		case .didReceiveTap:
			delegate?.statefulTableView(self, didTrigger: .didReceiveTapOnErrorStateView)
		}
	}
}
