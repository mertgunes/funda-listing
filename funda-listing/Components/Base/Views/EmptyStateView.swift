import UIKit
import SnapKit

final class EmptyStateView: UIView {
	weak var delegate: EmptyStateViewDelegate?
	
	private lazy var textLabel: UILabel = {
		let label = UILabel()
		label.textColor = ColorPalette.primaryTint.colorValue
		label.isUserInteractionEnabled = true
		label.textAlignment = .center
		label.numberOfLines = 0
		
		let selector = #selector(textLabelDidReceiveTap(_:))
		let recognizer = UITapGestureRecognizer(target: self, action: selector)
		label.addGestureRecognizer(recognizer)
		
		return label
	}()
	
	// MARK: - Initializers
	required init?(coder aDecoder: NSCoder) {
		preconditionFailure("init(coder:) has not been implemented")
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = ColorPalette.primaryBackground.colorValue
		
		addSubview(textLabel)
		textLabel.snp.makeConstraints { make in
			make.leading.trailing.equalToSuperview().inset(Constants.primaryMargin)
			make.centerY.equalToSuperview()
		}
	}
}

// MARK: - Configuration
extension EmptyStateView {
	func configure(with text: String) {
		textLabel.text = text
	}
}

// MARK: - Gesture
private extension EmptyStateView {
	@objc func textLabelDidReceiveTap(_ sender: Any) {
		delegate?.emptyStateView(self, didTrigger: .didReceiveTap)
	}
}

// MARK: - EmptyStateViewDelegate
extension EmptyStateView {
	
	enum DelegateAction {
		case didReceiveTap
	}
}

protocol EmptyStateViewDelegate: class {
	func emptyStateView(_ view: EmptyStateView, didTrigger action: EmptyStateView.DelegateAction)
}
