import UIKit
import SnapKit

final class ErrorStateView: UIView {
	weak var delegate: ErrorStateViewDelegate?
	
	private lazy var textLabel: UILabel = {
		let label = UILabel()
		label.textColor = ColorPalette.primaryTint.colorValue
		label.isUserInteractionEnabled = true
		label.textAlignment = .center
		label.numberOfLines = 0
		
		let selector = #selector(textLabelDidReceiveTap(_:))
		let recognizer = UITapGestureRecognizer(target: self, action: selector)
		label.addGestureRecognizer(recognizer)
		
		return label
	}()
	
	// MARK: - Initializers
	required init?(coder aDecoder: NSCoder) {
		preconditionFailure("init(coder:) has not been implemented")
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = ColorPalette.primaryBackground.colorValue
		
		addSubview(textLabel)
		textLabel.snp.makeConstraints { make in
			make.leading.trailing.equalToSuperview().inset(Constants.primaryMargin)
			make.centerY.equalToSuperview()
		}
	}
}

// MARK: - Configuration
extension ErrorStateView {
	func configure(with text: String) {
		textLabel.text = text
	}
}

// MARK: - Gesture
private extension ErrorStateView {
	@objc func textLabelDidReceiveTap(_ sender: Any) {
		delegate?.errorStateView(self, didTrigger: .didReceiveTap)
	}
}

// MARK: - ErrorStateViewDelegate
extension ErrorStateView {
	
	enum DelegateAction {
		case didReceiveTap
	}
}

protocol ErrorStateViewDelegate: class {
	func errorStateView(_ view: ErrorStateView, didTrigger action: ErrorStateView.DelegateAction)
}
