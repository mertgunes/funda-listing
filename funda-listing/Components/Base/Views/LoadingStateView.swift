import UIKit
import SnapKit

final class LoadingStateView: UIView {
	private lazy var activityIndicatorView: UIActivityIndicatorView = {
		let view = UIActivityIndicatorView(style: .whiteLarge)
		view.color = ColorPalette.primaryTint.colorValue
		return view
	}()
	
	// MARK: - Initializers
	required init?(coder aDecoder: NSCoder) {
		preconditionFailure("init(coder:) has not been implemented")
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = ColorPalette.primaryBackground.colorValue
		
		addSubview(activityIndicatorView)
		activityIndicatorView.snp.makeConstraints { make in
			make.center.equalToSuperview()
		}
	}
}

// MARK: - Animations
extension LoadingStateView {
	func startAnimating() {
		activityIndicatorView.startAnimating()
	}
	
	func stopAnimating() {
		activityIndicatorView.stopAnimating()
	}
}
