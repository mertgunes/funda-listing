import UIKit

class BaseNavigationController: UINavigationController {
	// MARK: - View Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.largeTitleDisplayMode = .automatic
		
		navigationBar.isTranslucent = false
		navigationBar.tintColor = UIColor.black
		navigationBar.barTintColor = UIColor.white
		navigationBar.prefersLargeTitles = true
		navigationBar.titleTextAttributes = TextAttributes.regular
		navigationBar.largeTitleTextAttributes = TextAttributes.large
	}
}

// MARK: - Text Attributes
private struct TextAttributes {
	public static var regular: [NSAttributedString.Key: Any] {
		var attributes = [NSAttributedString.Key: Any]()
		attributes[NSAttributedString.Key.font] = UIFont.preferredFont(forTextStyle: .headline)
		attributes[NSAttributedString.Key.foregroundColor] = UIColor.black
		return attributes
	}
	
	public static var large: [NSAttributedString.Key: Any] {
		var attributes = [NSAttributedString.Key: Any]()
		attributes[NSAttributedString.Key.font] = UIFont.preferredFont(forTextStyle: .largeTitle)
		attributes[NSAttributedString.Key.foregroundColor] = UIColor.black
		return attributes
	}
}
