import UIKit
import SnapKit

class BaseTableViewController: BaseViewController {
	lazy var contentView = StatefulTableView()
	
	// MARK: - View Lifecycle
	override func loadView() {
		super.loadView()
		
		view.addSubview(contentView)
		contentView.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
	}
}
