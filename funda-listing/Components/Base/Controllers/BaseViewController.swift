import UIKit
import RxSwift

class BaseViewController: UIViewController {
	lazy var disposeBag = DisposeBag()
	
	// MARK: - Initializers
	required init?(coder aDecoder: NSCoder) {
		preconditionFailure("init(coder:) has not been implemented")
	}
	
	init() {
		super.init(nibName: nil, bundle: nil)
	}
	
	// MARK: - View Lifecycle
	override func loadView() {
		super.loadView()
		view.backgroundColor = ColorPalette.primaryBackground.colorValue
	}
}
