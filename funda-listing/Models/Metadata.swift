import Foundation

struct Metadata {
	var description: String
	var objectType: String
	var title: String
}

private enum CodingKeys: String, CodingKey {
	case description	= "Omschrijving"
	case objectType		= "ObjectType"
	case title 			= "Titel"
}

// MARK: - Decodable
extension Metadata: Decodable {
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		description = try container.decode(String.self, forKey: .description)
		objectType = try container.decode(String.self, forKey: .objectType)
		title = try container.decode(String.self, forKey: .title)
	}
}
