import Foundation

struct EstateAgentListingCount {
	var estateAgent: EstateAgent
	var count: Int
}
