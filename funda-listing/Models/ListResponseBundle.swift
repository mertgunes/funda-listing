import Foundation

struct ListResponseBundle<T: Decodable>: Decodable {
	var objects: [T]
	var meta: Metadata
	var pagination: Pagination
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		objects = try container.decode([T].self, forKey: .objects)
		meta = try container.decode(Metadata.self, forKey: .metadata)
		pagination = try container.decode(Pagination.self, forKey: .pagination)
	}
}

private enum CodingKeys: String, CodingKey {
	case objects		= "Objects"
	case metadata		= "Metadata"
	case pagination		= "Paging"
}
