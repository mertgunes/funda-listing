import Foundation

private var defaultPageSize: Int = 25

struct Pagination {
	var totalPageCount: Int
	var currentPage: Int
	var nextPageUrlString: String?
	var previousPageUrlString: String?
	
	private let pageSize: Int
	
	init(totalPageCount: Int = 0, currentPage: Int = 0, pageSize: Int = defaultPageSize) {
		self.totalPageCount = totalPageCount
		self.currentPage = currentPage
		self.pageSize = pageSize
	}
	
	var isLastPage: Bool {
		if totalPageCount == 0, currentPage == 0 {
			return false
		}
		
		return currentPage == totalPageCount
	}
}

private enum CodingKeys: String, CodingKey {
	case totalPageCount			= "AantalPaginas"
	case currentPage			= "HuidigePagina"
	case nextPageUrlString 		= "VolgendeUrl"
	case previousPageUrlString 	= "VorigeUrl"
}

// MARK: - Decodable
extension Pagination: Decodable {
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		totalPageCount = try container.decode(Int.self, forKey: .totalPageCount)
		currentPage = try container.decode(Int.self, forKey: .currentPage)
		nextPageUrlString = try container.decode(String?.self, forKey: .nextPageUrlString)
		previousPageUrlString = try container.decode(String?.self, forKey: .previousPageUrlString)
		
		pageSize = defaultPageSize
	}
}

// MARK: - Parameters
extension Pagination {
	var nextPageParameters: [String: Any] {
		return [Property.page.rawValue: currentPage + 1,
				Property.pageSize.rawValue: pageSize]
	}
	
	private enum Property: String {
		case page		= "page"
		case pageSize	= "pagesize"
	}
}
