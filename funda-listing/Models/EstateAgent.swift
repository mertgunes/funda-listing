import Foundation

struct EstateAgent: Hashable {
	var identifier: Int64
	var name: String
}

extension EstateAgent: Equatable {
	static func ==(lhs: EstateAgent, rhs: EstateAgent) -> Bool {
		return (lhs.identifier == rhs.identifier && lhs.name == rhs.name)
	}
}
