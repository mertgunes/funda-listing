import Foundation

struct RentalListing {
	var globalIdentifier: Int64
	var identifier: String
	var estateAgent: EstateAgent
}

private enum CodingKeys: String, CodingKey {
	case globalIdentifier	= "GlobalId"
	case identifier			= "Id"
	case agentIdentifier	= "MakelaarId"
	case agentName			= "MakelaarNaam"
}

// MARK: - Decodable
extension RentalListing: Decodable {
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		globalIdentifier = try container.decode(Int64.self, forKey: .globalIdentifier)
		identifier = try container.decode(String.self, forKey: .identifier)
		let agentIdentifier = try container.decode(Int64.self, forKey: .agentIdentifier)
		let agentName = try container.decode(String.self, forKey: .agentName)
		estateAgent = EstateAgent(identifier: agentIdentifier, name: agentName)
	}
}
