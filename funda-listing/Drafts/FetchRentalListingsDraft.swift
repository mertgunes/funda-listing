import Foundation

struct FetchRentalListingsDraft {
	var location: String
	var type: ListingType
	var filter: Filter?
	var pagination: Pagination
	
	func parameters() -> [String: Any] {
		var parameters = [String: Any]()
		parameters[Property.location.rawValue]	= "/\(location)/" + (filter?.rawValue ?? "")
		parameters[Property.type.rawValue]		= type.rawValue
		return parameters <+ pagination.nextPageParameters
	}
}

extension FetchRentalListingsDraft {
	enum ListingType: String {
		case purchase	= "koop"
		case rent		= "huur"
	}
	
	enum Filter: String {
		case garden		= "tuin"
	}
	
	private enum Property: String {
		case location	= "zo"
		case type		= "type"
	}
}
