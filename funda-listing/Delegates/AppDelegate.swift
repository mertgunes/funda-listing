import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	
	private let rootCoordinator: RootCoordinator
	
	override init() {
		let urlString = "https://partnerapi.funda.nl"
		let apiKey = "ac1b0b1572524640a0ecc54de453ea9f"
		let networkClient = DefaultNetworkClient(with: urlString)
		let apiClient = DefaultAPIClient(networkClient: networkClient, and: apiKey)
		let repository = DefaultRepository(with: apiClient)
		
		self.rootCoordinator = RootCoordinator(repository: repository)
		
		super.init()
	}
	
	func application(_ application: UIApplication,
					 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		let window = UIWindow(frame: UIScreen.main.bounds)
		window.rootViewController = rootCoordinator.viewController
		window.makeKeyAndVisible()
		
		self.window = window
		
		return true
	}
}
