import UIKit

enum ColorPalette: String {
	case primaryBackground
	case primaryTint
	
	var colorValue: UIColor? {
		return UIColor(named: rawValue)
	}
}
